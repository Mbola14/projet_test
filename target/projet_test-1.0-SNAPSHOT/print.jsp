<%@ page import="java.util.HashMap" %>
<%@ page import="jakarta.servlet.ServletContext" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Vous avez entrer" %>
</h1>
<br/>
<%
    ServletContext sc = request.getServletContext();
    HashMap map = (HashMap) sc.getAttribute("urlMethod");
%>
<h3><% out.println((String)map.get("nom"));%></h3>
<p><% out.println((String)map.get("prenom"));%></p>
<br/>
<p><a href="newPersonne.do">Nouvelle personne</a></p>
</body>
</html>