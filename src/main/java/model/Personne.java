package model;

import utilitaire.ModelView;
import utilitaire.UrlServlet;

import java.util.HashMap;

public class Personne {
    String nom_personne;
    String prenom_personne;

     @UrlServlet(url = "savePersonne")
    public ModelView savePersonne(){
         HashMap<String, Object> map = new HashMap<String, Object>();
         map.put("nom",this.nom_personne);
         map.put("prenom",this.prenom_personne);

         System.out.println("Dept saved : "+this.nom_personne+", "+this.prenom_personne);
         ModelView mv = new ModelView("print.jsp", map);
         return mv;
     }

     @UrlServlet(url = "newPersonne")
    public ModelView newPersonne(){
         HashMap<String, Object> map = new HashMap<String, Object>();

         ModelView mv = new ModelView("newPersonne.jsp", map);
         return mv;
     }

     @UrlServlet(url = "printDept")
     public void printDept() {
         System.out.println("-------------> : Methode printDEPT");
     }

    public String getNom_dept() {
        return nom_personne;
    }

    public String getPrenom_dept() {
        return prenom_personne;
    }

    public void setNom_personne(String nom_dept) {
        this.nom_personne = nom_dept;
    }

    public void setPrenom_personne(String prenom_dept) {
        this.prenom_personne = prenom_dept;
    }
}
